# Goal: Creating a YouTube Clone 
## Creating React App
`npx create-react-app youtubeclone`
## Install Material-UI's source files 
`npm install @material-ui/core`
## Build React App
`npm run build`
## Deploy the App to Firebase
### Install Firebase CLI
`npm install -g firebase-tools`
### Connect To Google
`firebase login`
### Init the App
`firebase init`
### Deploy the App
`firebase deploy`
