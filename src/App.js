import React from 'react';
import './App.css';
import fetchYoutube from './API/FetchYouTube';


const App = () => {

    async function handleSubmit(searchTerm) {
        const { data } = await fetchYoutube.get("search", {
            params: {
                part: "snippet",
                maxResults: 5,
                key: 'AIzaSyCbBGKK8lfGnQhNsmkNpgR_fDc4D2Hi_MI',
                q: searchTerm,
              }
        });
        console.log(data);
    }


    return (
        <div className="container">
            <h1>Youtube Clone</h1>
            <button onClick={handleSubmit} >Fetch Video</button>
        </div>
    )
}

export default App
